INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
              
INSERT INTO format (id, br_ucesnika, tip)
	VALUES (1, 20, "GRAND_SLAM");
INSERT INTO format (id, br_ucesnika, tip)
	VALUES (2, 25, "MASTERS_1000");
INSERT INTO format (id, br_ucesnika, tip)
	VALUES (3, 30, "MASTERS_500");
INSERT INTO format (id, br_ucesnika, tip)
	VALUES (4, 35, "MASTERS_250");
	
INSERT INTO takmicenje (id, mesto_odrzavanja, naziv, pocetak, zavrsetak, format_id)
	VALUES (1, "Mesto 1", "Takmicenje 1", "2021-01-01", "2021-02-02", 1);
INSERT INTO takmicenje (id, mesto_odrzavanja, naziv, pocetak, zavrsetak, format_id)
	VALUES (2, "Naselje 1", "Nadmetanje 1", "2021-02-02", "2021-03-03", 2);
INSERT INTO takmicenje (id, mesto_odrzavanja, naziv, pocetak, zavrsetak, format_id)
	VALUES (3, "Mesto 2", "Takmicenje 2", "2021-03-03", "2021-04-04", 3);
INSERT INTO takmicenje (id, mesto_odrzavanja, naziv, pocetak, zavrsetak, format_id)
	VALUES (4, "Naselje 2", "Nadmetanje 2", "2021-04-04", "2021-05-05", 4);
INSERT INTO takmicenje (id, mesto_odrzavanja, naziv, pocetak, zavrsetak, format_id)
	VALUES (5, "Mesto 3", "Takmicenje 3", "2021-05-05", "2021-06-06", 1);

INSERT INTO prijava (id, datum_prijave, drzava_takmicara, email, takmicenje_id)
	VALUES (1, "2020-09-09", "AAA", "a@x.com", 1);
INSERT INTO prijava (id, datum_prijave, drzava_takmicara, email, takmicenje_id)
	VALUES (2, "2020-05-10", "BBB", "b@x.com", 1);
INSERT INTO prijava (id, datum_prijave, drzava_takmicara, email, takmicenje_id)
	VALUES (3, "2020-11-11", "CCC", "c@x.com", 1);
INSERT INTO prijava (id, datum_prijave, drzava_takmicara, email, takmicenje_id)
	VALUES (4, "2020-10-12", "BBB", "d@x.com", 2);
INSERT INTO prijava (id, datum_prijave, drzava_takmicara, email, takmicenje_id)
	VALUES (5, "2020-09-15", "AAA", "e@x.com", 2);