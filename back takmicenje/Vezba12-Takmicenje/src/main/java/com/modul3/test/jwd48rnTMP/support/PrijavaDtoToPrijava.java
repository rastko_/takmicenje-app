package com.modul3.test.jwd48rnTMP.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Prijava;
import com.modul3.test.jwd48rnTMP.service.PrijavaService;
import com.modul3.test.jwd48rnTMP.service.TakmicenjeService;
import com.modul3.test.jwd48rnTMP.web.dto.PrijavaDTO;

@Component
public class PrijavaDtoToPrijava implements Converter<PrijavaDTO, Prijava> {

	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Override
	public Prijava convert(PrijavaDTO dto) {
		Prijava prijava;
		if (dto.getId() != null) {
			prijava = prijavaService.findOne(dto.getId());
		} else {
			prijava = new Prijava();
		}
		if (prijava != null) {
			if (dto.getDatumPrijave() != null) {
				prijava.setDatumPrijave(getLocalDate(dto.getDatumPrijave()));
			}
			prijava.setDrzavaTakmicara(dto.getDrzavaTakmicara());
			prijava.setEmail(dto.getEmail());
			if (dto.getTakmicenjeId() != null) {
				prijava.setTakmicenje(takmicenjeService.findOne(dto.getTakmicenjeId()));
			}
		}
		return prijava;
	}

	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}
