package com.modul3.test.jwd48rnTMP.web.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Prijava;
import com.modul3.test.jwd48rnTMP.model.Takmicenje;
import com.modul3.test.jwd48rnTMP.service.PrijavaService;
import com.modul3.test.jwd48rnTMP.service.TakmicenjeService;
import com.modul3.test.jwd48rnTMP.support.PrijavaDtoToPrijava;
import com.modul3.test.jwd48rnTMP.support.PrijavaToPrijavaDto;
import com.modul3.test.jwd48rnTMP.web.dto.PrijavaDTO;

@RestController
@RequestMapping(value = "/api/prijave", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PrijavaController {

	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private PrijavaToPrijavaDto toPrijavaDto;
	
	@Autowired
	private PrijavaDtoToPrijava toPrijava;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<PrijavaDTO> getOne(@PathVariable Long id){
		Prijava prijava = prijavaService.findOne(id);
		if (prijava != null) {
			return new ResponseEntity<>(toPrijavaDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
//	@GetMapping
//	public ResponseEntity<List<PrijavaDTO>> getAll(){
//		List<Prijava> prijave = prijavaService.findAll();
//		return new ResponseEntity<>(toPrijavaDto.convert(prijave), HttpStatus.OK);
//	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<PrijavaDTO>> getAll(@RequestParam(required = false) Long takmicenjeId){
		if (takmicenjeId != null) {
			Takmicenje takmicenje = takmicenjeService.findOne(takmicenjeId);
			
			if (takmicenje == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			List<Prijava> prijave;
			
			prijave = prijavaService.findAllForTakmicenje(takmicenjeId);
			for (int i=0; i < prijave.size(); i++) {
				System.out.println(prijave.get(i));
			}
			
			return new ResponseEntity<>(toPrijavaDto.convert(prijave), HttpStatus.OK);	
		}
		List<Prijava> prijave = prijavaService.findAll();
		return new ResponseEntity<>(toPrijavaDto.convert(prijave), HttpStatus.OK);
	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
//	@GetMapping
//	public ResponseEntity<List<PrijavaDTO>> getAllForTakmicenje(
//			@RequestParam(required = false) Long takmicenjeId){
//		
//		Takmicenje takmicenje = takmicenjeService.findOne(takmicenjeId);
//		
//		if (takmicenje == null) {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//		
//		List<Prijava> prijave;
//		
//		prijave = prijavaService.findAllForTakmicenje(takmicenjeId);
//		
//		return new ResponseEntity<>(toPrijavaDto.convert(prijave), HttpStatus.OK);
//		
//	}
	
//	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrijavaDTO> create(@Valid @RequestBody PrijavaDTO prijavaDTO){
		System.out.println();
		System.out.println(prijavaDTO);
		System.out.println();
		
		Prijava prijava = toPrijava.convert(prijavaDTO);
		
		LocalDate danasnjiDatum = LocalDate.now();
		prijava.setDatumPrijave(danasnjiDatum);
		
		Prijava sacuvanaPrijava = prijavaService.save(prijava);
		return new ResponseEntity<>(toPrijavaDto.convert(sacuvanaPrijava), HttpStatus.CREATED);
	}
}
