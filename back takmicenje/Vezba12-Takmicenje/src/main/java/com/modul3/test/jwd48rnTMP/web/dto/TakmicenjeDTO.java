package com.modul3.test.jwd48rnTMP.web.dto;

import java.util.ArrayList;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class TakmicenjeDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@NotBlank (message = "Naziv - nije zadato.")
	@NotNull
	private String naziv;
	
	@NotBlank (message = "Mesto odrzavanja - nije zadato.")
	@NotNull
	@Size(max = 50)
	private String mestoOdrzavanja;
	
	@NotBlank (message = "Pocetak - nije zadato.")
	@NotNull
	@Pattern(regexp = "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$", message = "Datum nije validan.")
	private String pocetak;
	
	@NotBlank (message = "Zavrsetak - nije zadato.")
	@NotNull
	@Pattern(regexp = "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$", message = "Datum nije validan.")
	private String zavrsetak;
	
	private Long formatId;
	
	private String formatTip;
	
	private Integer brojMesta;
	
	private Integer brojSlobodnihMesta;

	private Integer brojPrijava;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getMestoOdrzavanja() {
		return mestoOdrzavanja;
	}

	public void setMestoOdrzavanja(String mestoOdrzavanja) {
		this.mestoOdrzavanja = mestoOdrzavanja;
	}

	public String getPocetak() {
		return pocetak;
	}

	public void setPocetak(String pocetak) {
		this.pocetak = pocetak;
	}

	public String getZavrsetak() {
		return zavrsetak;
	}

	public void setZavrsetak(String zavrsetak) {
		this.zavrsetak = zavrsetak;
	}

	public Long getFormatId() {
		return formatId;
	}

	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}

	public String getFormatTip() {
		return formatTip;
	}

	public void setFormatTip(String formatTip) {
		this.formatTip = formatTip;
	}

	public Integer getBrojPrijava() {
		return brojPrijava;
	}

	public void setBrojPrijava(Integer brojPrijava) {
		this.brojPrijava = brojPrijava;
	}

	public Integer getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(Integer brojMesta) {
		this.brojMesta = brojMesta;
	}
	
	public Integer getBrojSlobodnihMesta() {
		return brojSlobodnihMesta;
	}

	public void setBrojSlobodnihMesta(Integer brojSlobodnihMesta) {
		this.brojSlobodnihMesta = brojSlobodnihMesta;
	}

	@Override
	public String toString() {
		return "TakmicenjeDTO [id=" + id + ", naziv=" + naziv + ", mestoOdrzavanja=" + mestoOdrzavanja + ", pocetak="
				+ pocetak + ", zavrsetak=" + zavrsetak + ", formatId=" + formatId + ", formatTip=" + formatTip
				+ ", brojMesta=" + brojMesta + ", brojSlobodnihMesta=" + brojSlobodnihMesta + ", brojPrijava="
				+ brojPrijava + "]";
	}
	
	
	
	
}
