package com.modul3.test.jwd48rnTMP.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Long> {

	Format findOneById(Long id);
}
