package com.modul3.test.jwd48rnTMP.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;
import com.modul3.test.jwd48rnTMP.repository.TakmicenjeRepository;
import com.modul3.test.jwd48rnTMP.service.TakmicenjeService;

@Service
public class JpaTakmicenjeService implements TakmicenjeService {

	@Autowired
	private TakmicenjeRepository takmicenjeRepository;
	
	@Override
	public Takmicenje findOne(Long id) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.findOneById(id);
	}

	@Override
	public List<Takmicenje> findAll() {
		// TODO Auto-generated method stub
		return takmicenjeRepository.findAll();
	}

	@Override
	public List<Takmicenje> find(List<Long> ids) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.findByIdIn(ids);
	}

	@Override
	public Page<Takmicenje> findAll(int pageNo) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public Takmicenje save(Takmicenje takmicenje) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.save(takmicenje);
	}

	@Override
	public Takmicenje update(Takmicenje takmicenje) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.save(takmicenje);
	}

	@Override
	public Takmicenje delete(Long id) {
		Takmicenje takmicenjeDelete = findOne(id);
		if (takmicenjeDelete != null) {
			takmicenjeRepository.delete(takmicenjeDelete);
			return takmicenjeDelete;
		}
		return null;
	}

	@Override
	public Integer brojTakmicenjaPoFormatu(Long formatId) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.brojTakmicenjaPoFormatu(formatId);
	}

	@Override
	public Page<Takmicenje> find(Long formatId, String mestoOdrzavanja, LocalDate pocetakPretraga,
			LocalDate zavrsetakPretraga, int pageNo) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.search(formatId, mestoOdrzavanja, pocetakPretraga, zavrsetakPretraga, PageRequest.of(pageNo, 4));
	}

	@Override
	public List<Takmicenje> findAllNoPage(Long formatId, String mestoOdrzavanja, LocalDate pocetakPretraga,
			LocalDate zavrsetakPretraga) {
		// TODO Auto-generated method stub
		return takmicenjeRepository.searchTakmicenja(formatId, mestoOdrzavanja, pocetakPretraga, zavrsetakPretraga);
	}

}
