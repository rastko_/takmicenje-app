package com.modul3.test.jwd48rnTMP.enumeration;

public enum TipTakmicenja {
	GRAND_SLAM,
	MASTERS_1000,
	MASTERS_500,
	MASTERS_250
}
