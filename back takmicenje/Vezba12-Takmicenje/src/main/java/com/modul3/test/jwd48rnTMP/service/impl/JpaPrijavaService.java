package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Prijava;
import com.modul3.test.jwd48rnTMP.repository.PrijavaRepository;
import com.modul3.test.jwd48rnTMP.service.PrijavaService;

@Service
public class JpaPrijavaService implements PrijavaService {

	@Autowired
	private PrijavaRepository prijavaRepository;
	
	@Override
	public Prijava findOne(Long id) {
		// TODO Auto-generated method stub
		return prijavaRepository.findOneById(id);
	}

	@Override
	public List<Prijava> findAll() {
		// TODO Auto-generated method stub
		return prijavaRepository.findAll();
	}

	@Override
	public Prijava save(Prijava rezervacija) {
		// TODO Auto-generated method stub
		return prijavaRepository.save(rezervacija);
	}

	@Override
	public List<Prijava> findAllForTakmicenje(Long takmicenjeId) {
		
		return prijavaRepository.findAllForTakmicenje(takmicenjeId);
	}

}
