package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Prijava;
import com.modul3.test.jwd48rnTMP.web.dto.PrijavaDTO;

@Component
public class PrijavaToPrijavaDto implements Converter<Prijava, PrijavaDTO> {

	@Override
	public PrijavaDTO convert(Prijava prijava) {
		PrijavaDTO dto = new PrijavaDTO();
		dto.setId(prijava.getId());
		dto.setDatumPrijave(prijava.getDatumPrijave().toString());
		dto.setDrzavaTakmicara(prijava.getDrzavaTakmicara());
		dto.setEmail(prijava.getEmail());
		if (prijava.getTakmicenje() != null) {
			dto.setTakmicenjeId(prijava.getTakmicenje().getId());
		}
		return dto;
	}
	public List<PrijavaDTO> convert(List<Prijava> prijave){
		List<PrijavaDTO> dtos = new ArrayList<>();
		for (Prijava prijava : prijave) {
			dtos.add(convert(prijava));
		}
		return dtos;
	}
}
