package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Format;
import com.modul3.test.jwd48rnTMP.web.dto.FormatDTO;

@Component
public class FormatToFormatDto implements Converter<Format, FormatDTO> {

	@Override
	public FormatDTO convert(Format format) {
		FormatDTO dto = new FormatDTO();
		dto.setId(format.getId());
		dto.setBrUcesnika(format.getBrUcesnika());
		dto.setTip(format.getTip().toString());
		return dto;
	}
	public List<FormatDTO> convert(List<Format> formati){
		List<FormatDTO> dtos = new ArrayList<>();
		for (Format format : formati) {
			dtos.add(convert(format));
		}
		return dtos;
	}
}
