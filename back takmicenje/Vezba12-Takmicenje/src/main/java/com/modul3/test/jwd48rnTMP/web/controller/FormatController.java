package com.modul3.test.jwd48rnTMP.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Format;
import com.modul3.test.jwd48rnTMP.service.FormatService;
import com.modul3.test.jwd48rnTMP.support.FormatToFormatDto;
import com.modul3.test.jwd48rnTMP.web.dto.FormatDTO;

@RestController
@RequestMapping(value = "/api/formati", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class FormatController {

	@Autowired
	private FormatService formatService;
	
	@Autowired
	private FormatToFormatDto toFormatDto;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<FormatDTO> getOne(@PathVariable Long id){
		Format format = formatService.findOne(id);
		if (format != null) {
			return new ResponseEntity<>(toFormatDto.convert(format), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<FormatDTO>> getAll(){
		List<Format> formati = formatService.findAll();
		return new ResponseEntity<>(toFormatDto.convert(formati), HttpStatus.OK);
	}
}
