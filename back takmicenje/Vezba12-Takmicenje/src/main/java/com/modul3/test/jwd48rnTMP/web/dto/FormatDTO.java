package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.sun.istack.NotNull;

public class FormatDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@NotBlank (message = "Tip - nije zadato.")
	@NotNull
	private String tip;
	
	@Positive (message = "Broj ucesnika mora da bude pozitivan broj.")
	private Integer brUcesnika;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public Integer getBrUcesnika() {
		return brUcesnika;
	}

	public void setBrUcesnika(Integer brUcesnika) {
		this.brUcesnika = brUcesnika;
	}
	
	
}
