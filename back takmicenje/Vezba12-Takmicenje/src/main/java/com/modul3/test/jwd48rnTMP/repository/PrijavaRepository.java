package com.modul3.test.jwd48rnTMP.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Prijava;
import com.modul3.test.jwd48rnTMP.model.Takmicenje;

@Repository
public interface PrijavaRepository extends JpaRepository<Prijava, Long> {

	Prijava findOneById(Long id);
	
	@Query("SELECT p FROM Prijava p WHERE " +
            "(:takmicenjeId = NULL OR p.takmicenje.id = :takmicenjeId) " +
			"ORDER BY p.datumPrijave ASC")
	List<Prijava> findAllForTakmicenje(@Param("takmicenjeId")Long takmicenjeId);
}
