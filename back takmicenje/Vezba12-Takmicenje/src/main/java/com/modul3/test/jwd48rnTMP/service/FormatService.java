package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.Format;

public interface FormatService {

	Format findOne(Long id);
	
	List<Format> findAll();
	
	Format save(Format format);
}
