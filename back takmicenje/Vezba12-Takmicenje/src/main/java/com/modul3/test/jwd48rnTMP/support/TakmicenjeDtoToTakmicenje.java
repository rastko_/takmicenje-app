package com.modul3.test.jwd48rnTMP.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;
import com.modul3.test.jwd48rnTMP.service.FormatService;
import com.modul3.test.jwd48rnTMP.service.TakmicenjeService;
import com.modul3.test.jwd48rnTMP.web.dto.TakmicenjeDTO;

@Component
public class TakmicenjeDtoToTakmicenje implements Converter<TakmicenjeDTO, Takmicenje> {

	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Autowired
	private FormatService formatService;
	
	@Override
	public Takmicenje convert(TakmicenjeDTO dto) {
		Takmicenje takmicenje;
		if (dto.getId() != null) {
			takmicenje = takmicenjeService.findOne(dto.getId());
		} else {
			takmicenje = new Takmicenje();
		}
		if (takmicenje != null) {
			if (dto.getFormatId() != null) {
				takmicenje.setFormat(formatService.findOne(dto.getFormatId()));
			}
			takmicenje.setMestoOdrzavanja(dto.getMestoOdrzavanja());
			takmicenje.setNaziv(dto.getNaziv());
			takmicenje.setPocetak(getLocalDate(dto.getPocetak()));
			takmicenje.setZavrsetak(getLocalDate(dto.getZavrsetak()));
		}
		return takmicenje;
	}

	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}
