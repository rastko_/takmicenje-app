package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.Prijava;

public interface PrijavaService {

	Prijava findOne(Long id);
	
	List<Prijava> findAll();
	
	Prijava save(Prijava rezervacija);
	
	List<Prijava> findAllForTakmicenje(Long takmicenjeId);
}
