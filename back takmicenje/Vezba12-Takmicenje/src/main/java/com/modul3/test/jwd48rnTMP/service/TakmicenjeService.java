package com.modul3.test.jwd48rnTMP.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;

public interface TakmicenjeService {

	Takmicenje findOne(Long id);
	
	List<Takmicenje> findAll();
	
	List<Takmicenje> find(List<Long> ids);
	
	Page<Takmicenje> findAll(int pageNo);
	
	Takmicenje save(Takmicenje takmicenje);
	
	Takmicenje update(Takmicenje takmicenje);
	
	Takmicenje delete(Long id);

	Page<Takmicenje> find(Long formatId, String mestoOdrzavanja, LocalDate pocetakPretraga, LocalDate zavrsetakPretraga, int pageNo);
	
	Integer brojTakmicenjaPoFormatu (Long formatId);
	
	List<Takmicenje> findAllNoPage(Long formatId, String mestoOdrzavanja, LocalDate pocetakPretraga, LocalDate zavrsetakPretraga);
}
