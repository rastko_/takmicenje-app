package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Format;
import com.modul3.test.jwd48rnTMP.repository.FormatRepository;
import com.modul3.test.jwd48rnTMP.service.FormatService;

@Service
public class JpaFormatService implements FormatService {

	@Autowired
	private FormatRepository formatRepository;
	
	@Override
	public Format findOne(Long id) {
		// TODO Auto-generated method stub
		return formatRepository.findOneById(id);
	}

	@Override
	public List<Format> findAll() {
		// TODO Auto-generated method stub
		return formatRepository.findAll();
	}

	@Override
	public Format save(Format format) {
		// TODO Auto-generated method stub
		return formatRepository.save(format);
	}

}
