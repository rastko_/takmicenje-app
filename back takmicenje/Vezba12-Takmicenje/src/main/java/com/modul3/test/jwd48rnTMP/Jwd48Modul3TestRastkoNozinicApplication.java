package com.modul3.test.jwd48rnTMP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Jwd48Modul3TestRastkoNozinicApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(Jwd48Modul3TestRastkoNozinicApplication.class, args);
	}

}
