package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class PrijavaDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@NotBlank (message = "Drzava takmicara - nije zadato.")
	@NotNull
	@Size(min = 3, max = 3)
	private String drzavaTakmicara;
	
	@NotBlank (message = "Drzava takmicara - nije zadato.")
	@NotNull
	@Email
	private String email;
	
	private String datumPrijave;
	
	private Long takmicenjeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDrzavaTakmicara() {
		return drzavaTakmicara;
	}

	public void setDrzavaTakmicara(String drzavaTakmicara) {
		this.drzavaTakmicara = drzavaTakmicara;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDatumPrijave() {
		return datumPrijave;
	}

	public void setDatumPrijave(String datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

	public Long getTakmicenjeId() {
		return takmicenjeId;
	}

	public void setTakmicenjeId(Long takmicenjeId) {
		this.takmicenjeId = takmicenjeId;
	}

	@Override
	public String toString() {
		return "PrijavaDTO [id=" + id + ", drzavaTakmicara=" + drzavaTakmicara + ", email=" + email + ", datumPrijave="
				+ datumPrijave + ", takmicenjeId=" + takmicenjeId + "]";
	}
	
	
}
