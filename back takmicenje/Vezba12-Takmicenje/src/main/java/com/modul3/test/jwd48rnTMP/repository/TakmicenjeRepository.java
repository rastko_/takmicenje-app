package com.modul3.test.jwd48rnTMP.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;

@Repository
public interface TakmicenjeRepository extends JpaRepository<Takmicenje, Long> {

	Takmicenje findOneById(Long id);
	
	List<Takmicenje> findByIdIn(List<Long> ids);
	
	@Query("SELECT t FROM Takmicenje t WHERE " +
			"(:pocetakPretraga = NULL OR :zavrsetakPretraga = NULL OR (t.pocetak > :pocetakPretraga AND t.zavrsetak < :zavrsetakPretraga)) AND " +
            "(:mestoOdrzavanja = NULL OR t.mestoOdrzavanja LIKE %:mestoOdrzavanja%) AND " +
            "(:formatId = NULL OR t.format.id = :formatId) " +
			"ORDER BY t.pocetak DESC")
	Page<Takmicenje> search(@Param("formatId")Long formatId, @Param("mestoOdrzavanja")String mestoOdrzavanja, @Param("pocetakPretraga")LocalDate pocetakPretraga, @Param("zavrsetakPretraga")LocalDate zavrsetakPretraga, Pageable pageable);
	
	@Query(value = "SELECT COALESCE(COUNT(t.id), 0) FROM Takmicenje t WHERE " +
			"format_id = :formatId", nativeQuery = true)
	Integer brojTakmicenjaPoFormatu(@Param("formatId")Long formatId);
	
	@Query("SELECT t FROM Takmicenje t WHERE " +
			"(:pocetakPretraga = NULL OR :zavrsetakPretraga = NULL OR (t.pocetak > :pocetakPretraga AND t.zavrsetak < :zavrsetakPretraga)) AND " +
            "(:mestoOdrzavanja = NULL OR t.mestoOdrzavanja LIKE %:mestoOdrzavanja%) AND " +
            "(:formatId = NULL OR t.format.id = :formatId) " +
			"ORDER BY t.pocetak DESC")
	List<Takmicenje> searchTakmicenja(@Param("formatId")Long formatId, @Param("mestoOdrzavanja")String mestoOdrzavanja, @Param("pocetakPretraga")LocalDate pocetakPretraga, @Param("zavrsetakPretraga")LocalDate zavrsetakPretraga);
	
}
