package com.modul3.test.jwd48rnTMP.web.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;
import com.modul3.test.jwd48rnTMP.service.FormatService;
import com.modul3.test.jwd48rnTMP.service.PrijavaService;
import com.modul3.test.jwd48rnTMP.service.TakmicenjeService;
import com.modul3.test.jwd48rnTMP.support.TakmicenjeDtoToTakmicenje;
import com.modul3.test.jwd48rnTMP.support.TakmicenjeToTakmicenjeDto;
import com.modul3.test.jwd48rnTMP.web.dto.TakmicenjeDTO;

@RestController
@RequestMapping(value = "/api/takmicenja", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class TakmicenjeController {

	@Autowired
	private TakmicenjeService takmicenjeService;

	@Autowired
	private FormatService formatService;

	@Autowired
	private PrijavaService prijavaService;

	@Autowired
	private TakmicenjeToTakmicenjeDto toTakmicenjeDto;

	@Autowired
	private TakmicenjeDtoToTakmicenje toTakmicenje;

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<TakmicenjeDTO> getOne(@PathVariable Long id) {
		Takmicenje takmicenje = takmicenjeService.findOne(id);
		if (takmicenje != null) {
			return new ResponseEntity<>(toTakmicenjeDto.convert(takmicenje), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
/*
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TakmicenjeDTO>> getAll(@RequestParam(required = false) Long formatId,
			@RequestParam(required = false) String mestoOdrzavanja,
			@RequestParam(required = false) String pocetakString,
			@RequestParam(required = false) String zavrsetakString) {
//			@RequestParam(value="pageNo", defaultValue = "0") int pageNo){

//		Page<Takmicenje> pageTakmicenje;

		List<Takmicenje> takmicenja = new ArrayList<>();

		if (mestoOdrzavanja != null) {
			if (mestoOdrzavanja.trim().equals("")) {
				mestoOdrzavanja = null;
			}
		}

//		Long formatIdLong = null;
//		if (formatId != null && formatId != "-1") {
//			formatIdLong = Long.parseLong(formatId);
//		} 
		
		if (formatId != null) {
			if (formatId == -1) {
				formatId = null;
			}
		}
		
		LocalDate pocetakPretraga;
		LocalDate zavrsetakPretraga;
		if (pocetakString != null && pocetakString != "") {
			pocetakPretraga = getLocalDate(pocetakString);
		} else {
			pocetakPretraga = LocalDate.of(1900, 01, 01);
		}
		if (zavrsetakString != null && zavrsetakString != "") {
			zavrsetakPretraga = getLocalDate(zavrsetakString);
		} else {
			zavrsetakPretraga = LocalDate.of(3000, 01, 01);
		}

//		pageTakmicenje = takmicenjeService.find(formatId, mestoOdrzavanja, pocetakPretraga, zavrsetakPretraga, pageNo);
//		
//		
////		pageTakmicenje = takmicenjeService.findAll(pageNo);
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("total-pages", Integer.toString(pageTakmicenje.getTotalPages()));
//		
//		if (formatId != null) {
//			Integer brojTakmicenjaPoFormatu = takmicenjeService.brojTakmicenjaPoFormatu(formatId);
//			headers.add("br-takmicenja-format", brojTakmicenjaPoFormatu.toString());
//		}
//		
//		return new ResponseEntity<>(toTakmicenjeDto.convert(pageTakmicenje.getContent()), headers, HttpStatus.OK);
		System.out.println(formatId + " " + mestoOdrzavanja + " " + pocetakPretraga + " " + zavrsetakPretraga);

		takmicenja = takmicenjeService.findAllNoPage(formatId, mestoOdrzavanja, pocetakPretraga, zavrsetakPretraga);
		System.out.println(takmicenja.size());
		return new ResponseEntity<>(toTakmicenjeDto.convert(takmicenja), HttpStatus.OK);

	} */
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TakmicenjeDTO>> getAll(@RequestParam(required = false) Long formatId,
			@RequestParam(required = false) String mestoOdrzavanja,
			@RequestParam(required = false) String pocetakString,
			@RequestParam(required = false) String zavrsetakString,
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo){

		Page<Takmicenje> pageTakmicenje;

		if (mestoOdrzavanja != null) {
			if (mestoOdrzavanja.trim().equals("")) {
				mestoOdrzavanja = null;
			}
		}
		
		if (formatId != null) {
			if (formatId == -1) {
				formatId = null;
			}
		}
		
		LocalDate pocetakPretraga;
		LocalDate zavrsetakPretraga;
		if (pocetakString != null && pocetakString != "") {
			pocetakPretraga = getLocalDate(pocetakString);
		} else {
			pocetakPretraga = LocalDate.of(1900, 01, 01);
		}
		if (zavrsetakString != null && zavrsetakString != "") {
			zavrsetakPretraga = getLocalDate(zavrsetakString);
		} else {
			zavrsetakPretraga = LocalDate.of(3000, 01, 01);
		}

		pageTakmicenje = takmicenjeService.find(formatId, mestoOdrzavanja, pocetakPretraga, zavrsetakPretraga, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("total-pages", Integer.toString(pageTakmicenje.getTotalPages()));
//		
//		if (formatId != null) {
//			Integer brojTakmicenjaPoFormatu = takmicenjeService.brojTakmicenjaPoFormatu(formatId);
//			headers.add("br-takmicenja-format", brojTakmicenjaPoFormatu.toString());
//		}
//		
		return new ResponseEntity<>(toTakmicenjeDto.convert(pageTakmicenje.getContent()), headers, HttpStatus.OK);

	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicenjeDTO> create(@Valid @RequestBody TakmicenjeDTO takmicenjeDTO) {
		System.out.println();
		System.out.println(takmicenjeDTO);
		System.out.println();
		Takmicenje takmicenje = toTakmicenje.convert(takmicenjeDTO);
		if (takmicenje.getFormat() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Takmicenje sacuvanoTakmicenje = takmicenjeService.save(takmicenje);
		return new ResponseEntity<>(toTakmicenjeDto.convert(sacuvanoTakmicenje), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicenjeDTO> update(@PathVariable Long id,
			@Valid @RequestBody TakmicenjeDTO takmicenjeDTO) {
		if (takmicenjeDTO.getId() != id) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Takmicenje takmicenje = toTakmicenje.convert(takmicenjeDTO);
		if (takmicenje.getFormat() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Takmicenje updateTakmicenje = takmicenjeService.update(takmicenje);
		return new ResponseEntity<>(toTakmicenjeDto.convert(updateTakmicenje), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Takmicenje takmicenje = takmicenjeService.delete(id);
		System.out.println();
		System.out.println(takmicenje);
		System.out.println();
		if (takmicenje == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}
