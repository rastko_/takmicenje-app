package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Takmicenje;
import com.modul3.test.jwd48rnTMP.web.dto.TakmicenjeDTO;

@Component
public class TakmicenjeToTakmicenjeDto implements Converter<Takmicenje, TakmicenjeDTO> {

	@Override
	public TakmicenjeDTO convert(Takmicenje takmicenje) {
		TakmicenjeDTO dto = new TakmicenjeDTO();
		dto.setId(takmicenje.getId());
		if (takmicenje.getPrijave() != null) {
			dto.setBrojPrijava(takmicenje.getPrijave().size());
		} else {
			dto.setBrojPrijava(0);
		}
		if (takmicenje.getFormat() != null) {
			dto.setFormatId(takmicenje.getFormat().getId());
			dto.setFormatTip(takmicenje.getFormat().getTip().toString());
			dto.setBrojMesta(takmicenje.getFormat().getBrUcesnika());
		}
		dto.setMestoOdrzavanja(takmicenje.getMestoOdrzavanja());
		dto.setNaziv(takmicenje.getNaziv());
		dto.setPocetak(takmicenje.getPocetak().toString());
		dto.setZavrsetak(takmicenje.getZavrsetak().toString());
		dto.setBrojSlobodnihMesta(dto.getBrojMesta() - dto.getBrojPrijava());
		
		return dto;
	}
	public List<TakmicenjeDTO> convert(List<Takmicenje> takmicenja){
		List<TakmicenjeDTO> dtos = new ArrayList<>();
		for (Takmicenje takmicenje : takmicenja) {
			dtos.add(convert(takmicenje));
		}
		return dtos;
	}
	
}
