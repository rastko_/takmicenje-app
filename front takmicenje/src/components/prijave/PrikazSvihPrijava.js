import { Table } from "react-bootstrap";

const PrikazSvihPrijava = (props) => {
  const prijave = props.prijave;
  return (
    <Table striped bordered hover style={{ marginTop: 5 }}>
      <thead>
        <tr>
          <th>#</th>
          <th>E-mail</th>
          <th>Drzava takmicara</th>
          <th>Datum prijave</th>
        </tr>
      </thead>
      <tbody>
        {prijave.map((prijava, index) => (
          <tr key={index}>
            <td>{index + 1}</td>
            <td>{prijava.email}</td>
            <td>{prijava.drzavaTakmicara}</td>
            <td>{prijava.datumPrijave}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};
export default PrikazSvihPrijava;
