import { Fragment, useState } from "react";
import { Col, Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import useHttp from "../../hooks/use-http";
import useInput from "../../hooks/use-input";
import { addPrijava } from "../../lib/api";
import LoadingSpinner from "../UI/LoadingSpinner";
import { useDispatch } from "react-redux";
import { notificationActions } from "../../store/notification-slice";
import { useHistory } from "react-router";

const PrijavaForma = (props) => {
  const { sendRequest, error, status } = useHttp(addPrijava);

  const {
    value: enteredEmail,
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError,
    valueChangeHandler: emailChangedHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmailInput,
  } = useInput((value) => value.includes("@"));

  const {
    value: enteredDrzavaTakmicara,
    isValid: enteredDrzavaTakmicaraIsValid,
    hasError: drzavaTakmicaraInputHasError,
    valueChangeHandler: drzavaTakmicaraChangedHandler,
    inputBlurHandler: drzavaTakmicaraBlurHandler,
    reset: resetDrzavaTakmicaraInput,
  } = useInput((value) => value !== "" && value.length === 3);

  const history = useHistory();
  const dispatch = useDispatch();

  const [neispravanPokusajSubmittovanja, setNeispravanPokusajSubmittovanja] =
    useState(false);

  let formIsValid = false;

  if (enteredEmailIsValid && enteredDrzavaTakmicaraIsValid) {
    formIsValid = true;
  }

  const formSubmissionHandler = (e) => {
    e.preventDefault();

    if (!formIsValid) {
      setNeispravanPokusajSubmittovanja(true);
      return;
    }
    setNeispravanPokusajSubmittovanja(false);

    var trenutniDatum = new Date().toISOString().substring(0, 10);
    // console.log(new Date().toISOString());
    var addPrijavaData = {
      takmicenjeId: props.takmicenjeId,
      email: enteredEmail,
      drzavaTakmicara: enteredDrzavaTakmicara,
      datumPrijave: trenutniDatum,
    };
    // console.log(addPrijavaData);
    sendRequest(addPrijavaData);

    // resetEmailInput();
    // resetDrzavaTakmicaraInput();
    var newPrijava = { ...addPrijavaData, id: Math.random() };
    props.onEditTakmicenjeIPrijave(newPrijava);
    // props.onReloadPage(newPrijava);
  };

  var podnosenjeZahtevaIspis = undefined;

  if (status === "pending") {
    podnosenjeZahtevaIspis = (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    podnosenjeZahtevaIspis = (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
        }}
      >
        <p className="centered focused">{error}</p>
        <Link to={`/takmicenja`}>
          <Button variant="outline-dark" size="sm">
            Prikaz svih takmicenja
          </Button>
        </Link>
      </div>
    );
  }
  // if (status === "completed" && !error) {
  //   window.localStorage.setItem("korisnik-prijavljen", 1);
  //   podnosenjeZahtevaIspis = (
  //     <div
  //       style={{
  //         margin: "auto",
  //         textAlign: "center",
  //       }}
  //     >
  //       <p className="centered focused">
  //         Uspesno podnosenje prijave na takmicenje
  //       </p>
  //       <Link to={`/takmicenja`}>
  //         <Button variant="outline-dark" size="sm">
  //           Prikaz svih takmicenja
  //         </Button>
  //       </Link>
  //     </div>
  //   );
  // }
  

  if (status === "completed" && !error) {
    window.localStorage.setItem("korisnik-prijavljen", true);
    dispatch(
      notificationActions.createNotification({
        title: "USPEH",
        message: `Prijavili ste se na takmicenje ${props.takmicenjeNaziv} sa adresom ${enteredEmail}`,
      })
    );
    history.push("/takmicenja");
  }

  var ispisUzDugmeProvera =
    neispravanPokusajSubmittovanja ||
    emailInputHasError ||
    drzavaTakmicaraInputHasError;

  return (
    <Fragment>
      <Form style={{ marginTop: 35 }} onSubmit={formSubmissionHandler}>
        <Col>
          <Form.Group>
            <Form.Label>E-mail</Form.Label>
            <Form.Control
              id="email"
              name="email"
              value={enteredEmail}
              onChange={emailChangedHandler}
              onBlur={emailBlurHandler}
              isInvalid={emailInputHasError}
              as="input"
              type="email"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              E-mail mora da sadrzi znak "@".
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Drzava takmicara</Form.Label>
            <Form.Control
              id="drzavaTakmicara"
              name="drzavaTakmicara"
              value={enteredDrzavaTakmicara}
              onChange={drzavaTakmicaraChangedHandler}
              onBlur={drzavaTakmicaraBlurHandler}
              isInvalid={drzavaTakmicaraInputHasError}
              as="input"
              type="text"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Drzava takmicara mora da ima tacno 3 karaktera.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <br />
        <Col>
          <Form.Group>
            <Button variant="secondary" type="submit">
              Podnesi zahtev za prijavu
            </Button>
            {ispisUzDugmeProvera && (
              <span style={{ color: "red" }}>
                {" "}
                Niste ispravno uneli podatke
              </span>
            )}
          </Form.Group>
        </Col>
      </Form>
      {podnosenjeZahtevaIspis}
    </Fragment>
  );
};
export default PrijavaForma;
