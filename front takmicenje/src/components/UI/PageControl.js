import { ButtonGroup, Button } from "react-bootstrap";
import { ArrowLeft, ArrowRight } from "react-bootstrap-icons";

const PageControl = (props) => {
  const pageBackwards = () => {
    props.onChangePage(-1);
  };
  const pageForwards = () => {
    props.onChangePage(1);
  };
  return (
    <ButtonGroup className="float-right p-2">
      <Button
        variant="outline-dark"
        size="sm"
        disabled={props.pageParams.pageNo === 0}
        onClick={pageBackwards}
      >
        <ArrowLeft />
      </Button>
      <Button
        variant="outline-dark"
        size="sm"
        disabled={props.pageParams.pageNo === props.pageParams.totalPages - 1}
        onClick={pageForwards}
      >
        <ArrowRight />
      </Button>
    </ButtonGroup>
  );
};
export default PageControl;
