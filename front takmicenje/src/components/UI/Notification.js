import { useState } from "react";
import classes from "./Notification.module.css";
import { notificationActions } from "../../store/notification-slice";
import { useDispatch } from "react-redux";

const Notification = (props) => {
  const dispatch = useDispatch();

  const [cssClasses, setCssClasses] = useState(
    `${classes.notification} ${classes.show}`
  );
  setTimeout(() => {
    dispatch(notificationActions.removeNotification());
    setCssClasses(classes.notification);
  }, 3000);

  return (
    <section className={cssClasses}>
      <h2>{props.title}</h2>
      <p>{props.message}</p>
    </section>
  );
};
export default Notification;
