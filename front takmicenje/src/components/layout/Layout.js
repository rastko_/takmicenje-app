import { Container } from "react-bootstrap";
import { HashRouter } from "react-router-dom";
import MainNavigation from "./MainNavigation";

const Layout = (props) => {
  return (
    <HashRouter>
      <MainNavigation />
      <Container style={{ paddingTop: "25px" }}>
        <main>{props.children}</main>
      </Container>
    </HashRouter>
  );
};
export default Layout;
