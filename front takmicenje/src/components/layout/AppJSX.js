import { Fragment } from "react";
import { Redirect, Route, Switch } from "react-router";
import Login from "../../authorization/Login";
import EditTakmicenje from "../../pages/EditTakmicenje";
import NotFound from "../../pages/NotFound";
import NovoTakmicenje from "../../pages/NovoTakmicenje";
import SvaTakmicenja from "../../pages/SvaTakmicenja";
import Takmicenje from "../../pages/Takmicenje";

const AppJSX = () => {
  const jwt = window.localStorage["jwt"];
  var appJSX;
  if (!jwt) {
    appJSX = (
      <Switch>
        <Route path="/login" exact>
          <Login />
        </Route>
        <Route render={() => <Redirect to="/login" />} />
      </Switch>
    );
  } else {
    appJSX = (
      <Switch>
        <Route path="/" exact>
          <Redirect to="/takmicenja" />
        </Route>
        <Route path="/login" exact>
          <Redirect to="/takmicenja" />
        </Route>
        <Route path="/takmicenja" exact>
          <SvaTakmicenja />
        </Route>
        <Route path="/takmicenja/:takmicenjeId">
          <Takmicenje />
        </Route>
        <Route path="/izmena-takmicenja/:takmicenjeId" exact>
          <EditTakmicenje />
        </Route>
        <Route path="/dodaj-takmicenje" exact>
          <NovoTakmicenje />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    );
  }
  return <Fragment>{appJSX}</Fragment>;
};
export default AppJSX;
