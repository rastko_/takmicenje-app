import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { logout } from "../../lib/api";

const MainNavigation = () => {
  const logoutHandler = () => {
    logout();
  };
  return (
    <Navbar expand bg="dark" variant="dark">
      <Navbar.Brand as={Link} to="/">
        takmicenja + prijave
      </Navbar.Brand>
      <Nav>
        <Nav.Link as={Link} to="/takmicenja">
          Takmicenja
        </Nav.Link>
        {window.localStorage["role"] === "ROLE_ADMIN" && (
          <Nav.Link as={Link} to="/dodaj-takmicenje">
            Dodaj Takmicenje
          </Nav.Link>
        )}
        <Button onClick={logoutHandler} variant="secondary" size="sm">
          Logout
        </Button>
      </Nav>
    </Navbar>
  );
};
export default MainNavigation;
