import { useEffect, useState } from "react";
import { Col, Form } from "react-bootstrap";
import useHttp from "../../hooks/use-http";
import { getFormati } from "../../lib/api";
// import classes from "./PretragaTakmicenja.module.css";

const PretragaForma = (props) => {
  const [searchParams, setSearchParams] = useState({
    formatId: -1,
    mestoOdrzavanja: "",
    pocetakString: "",
    zavrsetakString: "",
    pageNo: 0
  });

  const { sendRequest, status, data: formati, error } = useHttp(getFormati);

  useEffect(() => {
    sendRequest();
  }, []);

  const onSearch = props.onSearch;

  const onChangeFunction = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    let newSearchParams = { ...searchParams };
    newSearchParams[name] = value;
    setSearchParams(newSearchParams);
    onSearch(newSearchParams);
  };

  return (
    <Form
      style={{ marginTop: 10, borderStyle: "dotted", borderWidth: "5px", borderColor: "#DCDCDC", borderRadius: "10px", padding: "10px" }}
      id="example-collapse-text"
    >
      <div className="row">
        <Col>
          {!error && status === "completed" && (
            <Form.Group>
              <Form.Label>Format</Form.Label>
              <Form.Control
                value={searchParams.formatId}
                onChange={onChangeFunction}
                name="formatId"
                as="select"
              >
                <option value={-1}></option>
                {formati.map((format) => (
                  <option value={format.id} key={format.id}>
                    {format.tip}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          )}
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Mesto odrzavanja</Form.Label>
            <Form.Control
              name="mestoOdrzavanja"
              value={searchParams.mestoOdrzavanja}
              onChange={onChangeFunction}
              as="input"
              type="text"
            ></Form.Control>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Pocetak odrzavanja</Form.Label>
            <Form.Control
              name="pocetakString"
              value={searchParams.pocetakString}
              onChange={onChangeFunction}
              as="input"
              type="date"
            ></Form.Control>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Zavrsetak odrzavanja</Form.Label>
            <Form.Control
              name="zavrsetakString"
              value={searchParams.zavrsetakString}
              onChange={onChangeFunction}
              as="input"
              type="date"
            ></Form.Control>
          </Form.Group>
        </Col>
      </div>
    </Form>
  );
};
export default PretragaForma;
