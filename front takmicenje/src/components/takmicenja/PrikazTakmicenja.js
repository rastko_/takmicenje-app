import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import useHttp from "../../hooks/use-http";
import { deleteTakmicenje } from "../../lib/api";

const PrikazTakmicenja = (props) => {
  const takmicenje = props.takmicenje;
  const { sendRequest } = useHttp(deleteTakmicenje);
  const brisanjeHandler = () => {
    sendRequest(takmicenje.id);
    props.onRemoveTakmicenjeFromState(takmicenje.id);
  };
  const prikaziFormuPrijavaHandler = () => {
    props.onPrikaziFormuPrijava();
  };
  return (
    <tr>
      <td>{takmicenje.naziv}</td>
      <td>{takmicenje.mestoOdrzavanja}</td>
      <td>{takmicenje.pocetak}</td>
      <td>{takmicenje.zavrsetak}</td>
      <td>{takmicenje.formatTip}</td>
      <td>{takmicenje.brojSlobodnihMesta}</td>
      {props.svaTakmicenja && (
        <td>
          <Link to={`/takmicenja/${takmicenje.id}`}>
            <Button variant="info" size="sm">
              Prijave
            </Button>
          </Link>
        </td>
      )}
      {props.svaTakmicenja && window.localStorage["role"] === "ROLE_ADMIN" && (
        <td>
          <Button onClick={brisanjeHandler} variant="outline-danger" size="sm">
            Brisanje
          </Button>
        </td>
      )}
      {props.jednoTakmicenje && window.localStorage["role"] === "ROLE_KORISNIK" && (
        <td>
          <Button
            disabled={takmicenje.brojSlobodnihMesta <= 0 || window.localStorage["korisnik-prijavljen"] === "true"}
            onClick={prikaziFormuPrijavaHandler}
            variant="outline-danger"
            size="sm"
          >
            Prijavi se
          </Button>
        </td>
      )}
      {window.localStorage["role"] === "ROLE_ADMIN" && (
        <td>
          <Link to={`/izmena-takmicenja/${takmicenje.id}`}>
            <Button variant="outline-primary" size="sm">
              Izmena
            </Button>
          </Link>
        </td>
      )}
    </tr>
  );
};
export default PrikazTakmicenja;
