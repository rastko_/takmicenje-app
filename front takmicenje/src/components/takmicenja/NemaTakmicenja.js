import { Alert, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const NemaTakmicenja = () => {
  return (
    <Alert
      className="text-center"
      style={{
        marginTop: 200,
        borderRadius: "50%",
        width: "300px",
        height: "300px",
        margin: "auto",
        padding: "100px 0",
        textAlign: "center",
        background: "transparent"
      }}
    >
      <div>
        <p>Nema pronadjenih takmicenja!</p>
        <Link to="/dodaj-takmicenje">
          <Button variant="outline-dark" size="lg">
            Dodaj takmicenje
          </Button>
        </Link>
      </div>
    </Alert>
  );
};

export default NemaTakmicenja;
