import { Fragment, useState } from "react";
import { Form } from "react-bootstrap";
import PretragaForma from "./PretragaForma";

const Pretraga = (props) => {
  const [vidljivost, setVidljivost] = useState(false);

  const changeVidljivostHandler = () => {
    if (vidljivost) {
      props.onResetSearchParams();
    }
    setVidljivost((prevVidljivost) => {
      return !prevVidljivost;
    });
  };

  return (
    <Fragment>
      <Form.Group controlId="formBasicCheckbox">
        <Form.Check
          onChange={changeVidljivostHandler}
          type="checkbox"
          label={`${vidljivost ? "Sakrij" : "Prikazi"} formu za pretragu`}
        />
      </Form.Group>
      {vidljivost && (
        <PretragaForma
          currentSearchParams={props.currentSearchParams}
          onSearch={props.onSearch}
        />
      )}
    </Fragment>
  );
};
export default Pretraga;
