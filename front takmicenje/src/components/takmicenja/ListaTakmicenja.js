import { Fragment } from "react";
import { Table } from "react-bootstrap";
import PrikazTakmicenja from "./PrikazTakmicenja";

// import classes from "./ListaTakmicenja.module.css";

const ListaTakmicenja = (props) => {
  return (
    <Fragment>
      <Table striped bordered hover style={{ marginTop: 5 }}>
        <thead style={{ backgroundColor: "#2A3834", color: "white" }}>
          <tr>
            <th>Naziv</th>
            <th>Mesto odrzavanja</th>
            <th>Pocetak</th>
            <th>Zavrsetak</th>
            <th>Format</th>
            <th>Broj slobodnih mesta</th>
            <th>Detalji</th>
            {window.localStorage["role"] === "ROLE_ADMIN" && <th>Brisanje</th>}
            {window.localStorage["role"] === "ROLE_ADMIN" && <th>Izmena</th>}
          </tr>
        </thead>
        <tbody>
          {props.takmicenja.map((takmicenje) => (
            <PrikazTakmicenja
              svaTakmicenja={true}
              onRemoveTakmicenjeFromState={props.onRemoveTakmicenjeFromState}
              key={takmicenje.id}
              takmicenje={takmicenje}
            />
          ))}
        </tbody>
      </Table>
    </Fragment>
  );
};
export default ListaTakmicenja;
