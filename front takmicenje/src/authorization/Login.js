import { useState } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import { useHistory } from "react-router";
import { login } from "../lib/api";

const Login = () => {
  const [loginData, setLoginData] = useState({
    username: "",
    password: "",
  });

  const history = useHistory();

  const onChangeFunction = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    let newLoginData = { ...loginData };
    newLoginData[name] = value;
    setLoginData(newLoginData);
  };

  const formSubmitHandler = (e) => {
    e.preventDefault();
    login(loginData);
  };

  return (
    <Row className="justify-content-center">
      <Col md={6}>
        <Form onSubmit={formSubmitHandler}>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              value={loginData.username}
              as="input"
              type="text"
              name="username"
              onChange={onChangeFunction}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={loginData.password}
              as="input"
              type="password"
              name="password"
              onChange={onChangeFunction}
            />
          </Form.Group>
          <Form.Group>
            <Button style={{ marginTop: 25 }} variant="success" type="submit">
              Login
            </Button>
          </Form.Group>
        </Form>
      </Col>
    </Row>
  );
};
export default Login;
