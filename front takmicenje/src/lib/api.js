import jwt_decode from "jwt-decode";

const TAKMICENJE_DOMAIN = "http://localhost:8080/api";
// 'https://multi-page-spa-quotes-default-rtdb.firebaseio.com';

// export async function getTakmicenja() {
//   const response = await fetch(`${TAKMICENJE_DOMAIN}/takmicenja`);
//   const data = await response.json();
//   console.log(data);

//   if (!response.ok) {
//     throw new Error(data.message || 'Could not fetch takmicenja.');
//   }

//   const transformedTakmicenja = [];

//   for (const key in data) {
//     const takmicenjeObj = {
//       ...data[key],
//     };

//     transformedTakmicenja.push(takmicenjeObj);
//   }
//   console.log(transformedTakmicenja);

//   return transformedTakmicenja;
// }

const jwt = window.localStorage["jwt"];
var bearerHeader = {};
if (jwt) {
  bearerHeader = {
    Authorization: "Bearer " + jwt,
  };
}

export async function getTakmicenja(searchParams) {
  const response = await fetch(
    `${TAKMICENJE_DOMAIN}/takmicenja/?` +
      new URLSearchParams(searchParams).toString(),
    {
      headers: bearerHeader,
    }
  );
  const data = await response.json();
  const totalPages = response.headers.get("total-pages");

  if (!response.ok) {
    throw new Error(data.message || "Could not fetch takmicenja.");
  }

  const takmicenjaPage = { totalPages: totalPages, takmicenja: data };

  return takmicenjaPage;
}

export async function getFormati() {
  const response = await fetch(`${TAKMICENJE_DOMAIN}/formati`, {
    headers: bearerHeader,
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not fetch formate.");
  }
  // const transformedFormati = [];

  // for (const key in data) {
  //   const formatObj = {
  //     ...data[key],
  //   };

  //   transformedFormati.push(formatObj);
  // }

  return data;
}

export async function getPrijave() {
  const response = await fetch(`${TAKMICENJE_DOMAIN}/prijave`, {
    headers: bearerHeader,
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not fetch prijave.");
  }

  return data;
}

export async function deleteTakmicenje(takmicenjeId) {
  const response = await fetch(
    `${TAKMICENJE_DOMAIN}/takmicenja/${takmicenjeId}`,
    {
      method: "DELETE",
      headers: bearerHeader,
    }
  );
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not delete takmicenje.");
  }
  console.log(data);

  return data;
}

export async function addPrijava(prijavaData) {
  var headers = { ...bearerHeader, "Content-Type": "application/json" };
  const response = await fetch(`${TAKMICENJE_DOMAIN}/prijave`, {
    method: "POST",
    body: JSON.stringify(prijavaData),
    headers: headers,
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not create prijava.");
  }

  return null;
}

export async function addTakmicenje(takmicenjeData) {
  var headers = { ...bearerHeader, "Content-Type": "application/json" };
  const response = await fetch(`${TAKMICENJE_DOMAIN}/takmicenja`, {
    method: "POST",
    body: JSON.stringify(takmicenjeData),
    headers: headers,
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not create takmicenje.");
  }

  return null;
}

export async function getTakmicenjeIPrijave(takmicenjeId) {
  const responseTakmicenje = await fetch(
    `${TAKMICENJE_DOMAIN}/takmicenja/${takmicenjeId}`,
    {
      headers: bearerHeader,
    }
  );
  const dataTakmicenje = await responseTakmicenje.json();

  if (!responseTakmicenje.ok) {
    throw new Error(dataTakmicenje.message || "Could not fetch takmicenje.");
  }

  const prijavaSearchParams = {
    takmicenjeId: takmicenjeId,
  };

  const responsePrijave = await fetch(
    `${TAKMICENJE_DOMAIN}/prijave/?` +
      new URLSearchParams(prijavaSearchParams).toString(),
    {
      headers: bearerHeader,
    }
  );
  const dataPrijave = await responsePrijave.json();

  if (!responsePrijave.ok) {
    throw new Error(dataTakmicenje.message || "Could not fetch prijave.");
  }

  const data = {
    takmicenje: { ...dataTakmicenje },
    prijave: [...dataPrijave],
  };

  return data;
}

export async function getTakmicenje(takmicenjeId) {
  const responseTakmicenje = await fetch(
    `${TAKMICENJE_DOMAIN}/takmicenja/${takmicenjeId}`,
    {
      headers: bearerHeader,
    }
  );
  const dataTakmicenje = await responseTakmicenje.json();

  if (!responseTakmicenje.ok) {
    throw new Error(dataTakmicenje.message || "Could not fetch takmicenje.");
  }

  return dataTakmicenje;
}

export async function editTakmicenje(takmicenjeData) {
  var headers = { ...bearerHeader, "Content-Type": "application/json" };
  const response = await fetch(
    `${TAKMICENJE_DOMAIN}/takmicenja/${takmicenjeData.id}`,
    {
      method: "PUT",
      body: JSON.stringify(takmicenjeData),
      headers: headers,
    }
  );
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not edit takmicenje.");
  }

  return null;
}

export async function getSingleQuote(quoteId) {
  const response = await fetch(`${TAKMICENJE_DOMAIN}/quotes/${quoteId}.json`);
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not fetch quote.");
  }

  const loadedQuote = {
    id: quoteId,
    ...data,
  };

  return loadedQuote;
}

export async function addQuote(quoteData) {
  const response = await fetch(`${TAKMICENJE_DOMAIN}/quotes.json`, {
    method: "POST",
    body: JSON.stringify(quoteData),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not create quote.");
  }

  return null;
}

export async function addComment(requestData) {
  const response = await fetch(
    `${TAKMICENJE_DOMAIN}/comments/${requestData.quoteId}.json`,
    {
      method: "POST",
      body: JSON.stringify(requestData.commentData),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not add comment.");
  }

  return { commentId: data.name };
}

export async function getAllComments(quoteId) {
  const response = await fetch(`${TAKMICENJE_DOMAIN}/comments/${quoteId}.json`);

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Could not get comments.");
  }

  const transformedComments = [];

  for (const key in data) {
    const commentObj = {
      id: key,
      ...data[key],
    };

    transformedComments.push(commentObj);
  }

  return transformedComments;
}

export async function login(loginData) {
  console.log(loginData);
  const response = await fetch(`${TAKMICENJE_DOMAIN}/korisnici/auth`, {
    method: "POST",
    body: JSON.stringify(loginData),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });
  console.log(response);
  // const data = await response.json();
  const data = await response.text();
  console.log(data);

  if (!response.ok) {
    throw new Error(data.message || "Could not login.");
  }

  const decoded = await jwt_decode(data);
  console.log(decoded);

  window.localStorage.setItem("role", decoded.role.authority);
  window.localStorage.setItem("jwt", data);

  window.location.reload();
}

export const logout = () => {
  window.localStorage.removeItem("role");
  window.localStorage.removeItem("jwt");
  window.localStorage.removeItem("prijavljen");
  window.location.reload();
};
