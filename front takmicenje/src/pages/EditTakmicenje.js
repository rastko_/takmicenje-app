import { Fragment, useEffect, useState } from "react";
import { Col, Button, Form } from "react-bootstrap";
import { useHistory, useParams } from "react-router";
import { Link } from "react-router-dom";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import useInput from "../hooks/use-input";
import { editTakmicenje, getFormati, getTakmicenje } from "../lib/api";
import { useDispatch } from "react-redux";
import { notificationActions } from "../store/notification-slice";

const EditTakmicenje = (props) => {
  const params = useParams();

  const getTakmicenjeHttp = useHttp(getTakmicenje);
  const editTakmicenjeHttp = useHttp(editTakmicenje);
  const getFormatiHttp = useHttp(getFormati);
  const [neispravanPokusajSubmittovanja, setNeispravanPokusajSubmittovanja] =
    useState(false);

  const history = useHistory();
  const dispatch = useDispatch();

  const proveraIspravnostiDatuma = (date1, date2) => {
    if (date2 === null) {
      var formatDate0 = new Date();
      var formatDate1 = new Date(date1);
      if (formatDate1 < formatDate0) {
        return false;
      }
      return true;
    } else {
      formatDate1 = new Date(date1);
      var formatDate2 = new Date(date2);
      if (formatDate2 < formatDate1) {
        return false;
      }
      return true;
    }
  };

  const {
    value: enteredNaziv,
    isValid: enteredNazivIsValid,
    hasError: nazivInputHasError,
    valueChangeHandler: nazivChangedHandler,
    inputBlurHandler: nazivBlurHandler,
    reset: resetNazivInput,
    setEnteredValue: setNaziv,
  } = useInput((value) => value !== "");

  const {
    value: enteredMestoOdrzavanja,
    isValid: enteredMestoOdrzavanjaIsValid,
    hasError: mestoOdrzavanjaInputHasError,
    valueChangeHandler: mestoOdrzavanjaChangedHandler,
    inputBlurHandler: mestoOdrzavanjaBlurHandler,
    reset: resetMestoOdrzavanjaInput,
    setEnteredValue: setMestoOdrzavanja,
  } = useInput((value) => value !== "");

  const {
    value: enteredPocetak,
    isValid: enteredPocetakIsValid,
    hasError: pocetakInputHasError,
    valueChangeHandler: pocetakChangedHandler,
    inputBlurHandler: pocetakBlurHandler,
    reset: resetPocetakInput,
    setEnteredValue: setPocetak,
  } = useInput((value) => value !== "" && proveraIspravnostiDatuma(value));

  const {
    value: enteredZavrsetak,
    isValid: enteredZavrsetakIsValid,
    hasError: zavrsetakInputHasError,
    valueChangeHandler: zavrsetakChangedHandler,
    inputBlurHandler: zavrsetakBlurHandler,
    reset: resetZavrsetakInput,
    setEnteredValue: setZavrsetak,
  } = useInput(
    (value) => value !== "" && proveraIspravnostiDatuma(enteredPocetak, value)
  );

  const {
    value: enteredFormatId,
    isValid: enteredFormatIdIsValid,
    hasError: formatIdInputHasError,
    valueChangeHandler: formatIdChangedHandler,
    inputBlurHandler: formatIdBlurHandler,
    reset: resetFormatIdInput,
    setEnteredValue: setFormatId,
  } = useInput((value) => value !== -1 && value > 0);

  useEffect(() => {
    getTakmicenjeHttp.sendRequest(params.takmicenjeId);
    getFormatiHttp.sendRequest();
  }, []);

  const getTakmicenjeHttpData = getTakmicenjeHttp.data;
  const getFormatiHttpData = getFormatiHttp.data;
  useEffect(() => {
    if (getTakmicenjeHttpData !== null && getFormatiHttpData !== null) {
      setNaziv(getTakmicenjeHttpData.naziv);
      setMestoOdrzavanja(getTakmicenjeHttpData.mestoOdrzavanja);
      setPocetak(getTakmicenjeHttpData.pocetak);
      setZavrsetak(getTakmicenjeHttpData.zavrsetak);
      setFormatId(getTakmicenjeHttpData.formatId);
    }
  }, [
    getTakmicenjeHttpData,
    getFormatiHttpData,
    setNaziv,
    setMestoOdrzavanja,
    setPocetak,
    setZavrsetak,
    setFormatId,
  ]);

  var podnosenjeZahtevaIspis = undefined;

  if (editTakmicenjeHttp.status === "pending") {
    podnosenjeZahtevaIspis = (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }
  if (editTakmicenjeHttp.error) {
    podnosenjeZahtevaIspis = (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
        }}
      >
        <p className="centered focused">{editTakmicenjeHttp.error}</p>
        <Link to={`/takmicenja`}>
          <Button variant="outline-dark" size="sm">
            Prikaz svih takmicenja
          </Button>
        </Link>
      </div>
    );
  }
  if (editTakmicenjeHttp.status === "completed" && !editTakmicenjeHttp.error) {
    dispatch(
      notificationActions.createNotification({
        title: "USPEH",
        message: "Uspesno ste izmenili takmicenje.",
      })
    );
    history.push("/takmicenja");
  }

  var ispisUzDugmeProvera =
    neispravanPokusajSubmittovanja ||
    nazivInputHasError ||
    mestoOdrzavanjaInputHasError ||
    pocetakInputHasError ||
    zavrsetakInputHasError ||
    formatIdInputHasError;

  // const getTodayDateString = () => {
  //   var todayFullDate = new Date();
  //   var todayDateString = todayFullDate.toISOString().substring(0, 10);
  //   return todayDateString;
  // };

  let formIsValid = false;

  if (
    enteredNazivIsValid &&
    enteredMestoOdrzavanjaIsValid &&
    enteredPocetakIsValid &&
    enteredZavrsetakIsValid &&
    enteredFormatIdIsValid
  ) {
    formIsValid = true;
  }

  const formSubmissionHandler = (e) => {
    e.preventDefault();

    if (!formIsValid) {
      setNeispravanPokusajSubmittovanja(true);
      return;
    }
    setNeispravanPokusajSubmittovanja(false);

    var editTakmicenjeData = {
      id: getTakmicenjeHttp.data.id,
      naziv: enteredNaziv,
      mestoOdrzavanja: enteredMestoOdrzavanja,
      pocetak: enteredPocetak,
      zavrsetak: enteredZavrsetak,
      formatId: enteredFormatId,
    };
    editTakmicenjeHttp.sendRequest(editTakmicenjeData);

    resetNazivInput();
    resetMestoOdrzavanjaInput();
    resetPocetakInput();
    resetZavrsetakInput();
    resetFormatIdInput();
  };

  if (getFormatiHttp.error || getTakmicenjeHttp.error) {
    return (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
        }}
      >
        <p className="centered focused">
          Trenutno ne mozete da izmenite takmicenje
        </p>
        <Link to={`/takmicenja`}>
          <Button variant="outline-dark" size="sm">
            Prikaz svih takmicenja
          </Button>
        </Link>
      </div>
    );
  }

  return (
    <Fragment>
      <Form style={{ marginTop: 35 }} onSubmit={formSubmissionHandler}>
        <Col>
          <Form.Group>
            <Form.Label>Naziv takmicenja</Form.Label>
            <Form.Control
              id="naziv"
              name="naziv"
              value={enteredNaziv}
              onChange={nazivChangedHandler}
              onBlur={nazivBlurHandler}
              isInvalid={nazivInputHasError}
              as="input"
              type="text"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Naziv takmicenja ne moze da bude prazno polje.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Mesto odrzavanja</Form.Label>
            <Form.Control
              id="mestoOdrzavanja"
              name="mestoOdrzavanja"
              value={enteredMestoOdrzavanja}
              onChange={mestoOdrzavanjaChangedHandler}
              onBlur={mestoOdrzavanjaBlurHandler}
              isInvalid={mestoOdrzavanjaInputHasError}
              as="input"
              type="text"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Mesto odrzavanja ne moze da bude prazno polje.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Datum pocetka takmicenja</Form.Label>
            <Form.Control
              id="pocetak"
              name="pocetak"
              value={enteredPocetak}
              onChange={pocetakChangedHandler}
              onBlur={pocetakBlurHandler}
              isInvalid={pocetakInputHasError}
              min={enteredPocetak}
              as="input"
              type="date"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Pocetak ne moze da bude nedefinisan, niti datum pre danasnjeg
              dana.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Datum zavrsetka takmicenja</Form.Label>
            <Form.Control
              disabled={enteredPocetak === ""}
              id="zavrsetak"
              name="zavrsetak"
              value={enteredZavrsetak}
              onChange={zavrsetakChangedHandler}
              onBlur={zavrsetakBlurHandler}
              isInvalid={zavrsetakInputHasError}
              min={enteredPocetak}
              as="input"
              type="date"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Zavrsetak ne moze da bude nedefinisan, niti datum pre dana pocetka
              takmicenja.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Format takmicenja</Form.Label>
            {!getFormatiHttp.error && getFormatiHttp.status === "completed" && (
              <Form.Control
                value={enteredFormatId}
                onChange={formatIdChangedHandler}
                onBlur={formatIdBlurHandler}
                isInvalid={formatIdInputHasError}
                id="formatId"
                name="formatId"
                as="select"
              >
                <option value={-1}></option>
                {getFormatiHttp.data.map((format) => (
                  <option value={format.id} key={format.id}>
                    {format.tip}
                  </option>
                ))}
              </Form.Control>
            )}

            <Form.Control.Feedback type="invalid" tooltip>
              Odaberite jedan od ponudjenih formata.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <br />
        <Col>
          <Form.Group>
            <Button variant="secondary" type="submit">
              Podnesi zahtev za izmenu takmicenja
            </Button>
            {ispisUzDugmeProvera && (
              <span style={{ color: "red" }}>
                {" "}
                Niste ispravno uneli podatke
              </span>
            )}
          </Form.Group>
        </Col>
      </Form>
      {podnosenjeZahtevaIspis}
    </Fragment>
  );
};
export default EditTakmicenje;
