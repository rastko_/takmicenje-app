import React, { Fragment, useEffect, useState } from "react";
import ListaTakmicenja from "../components/takmicenja/ListaTakmicenja";
import useHttp from "../hooks/use-http";
import { getTakmicenja } from "../lib/api";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import NemaTakmicenja from "../components/takmicenja/NemaTakmicenja";
import Pretraga from "../components/takmicenja/Pretraga";
import PageControl from "../components/UI/PageControl";

const SvaTakmicenja = () => {
  const defaultSearchParams = {
    formatId: "-1",
    mestoOdrzavanja: "",
    pocetakString: "",
    zavrsetakString: "",
  };

  const { editData, sendRequest, status, data, error } = useHttp(
    getTakmicenja,
    true
  );

  const [pageParams, setPageParams] = useState({ pageNo: 0, totalPages: 1 });
  const [currentSearchParams, setCurrentSearchParams] =
    useState(defaultSearchParams);

  const searchHandler = (searchParams) => {
    sendRequest(searchParams);
    setCurrentSearchParams(searchParams);
  };

  const changePageHandler = (change) => {
    var newSearchParams = { ...currentSearchParams };
    newSearchParams["pageNo"] = pageParams.pageNo + change;

    setPageParams((prevPageParams) => {
      return { ...prevPageParams, pageNo: prevPageParams.pageNo + change };
    });
    sendRequest(newSearchParams);
  };

  const resetSearchParamsHandler = () => {
    setCurrentSearchParams(defaultSearchParams);
    sendRequest(defaultSearchParams);
  };

  useEffect(() => {
    sendRequest(defaultSearchParams);
  }, []);

  useEffect(() => {
    if (data !== null && data.totalPages !== null) {
      setPageParams((prevPageParams) => {
        return { ...prevPageParams, totalPages: data.totalPages };
      });
    }
  }, [data]);

  const removeTakmicenjeFromStateHandler = (takmicenjeId) => {
    var newTakmicenja = data.takmicenja.filter((obj) => obj.id !== takmicenjeId)
    var newData = {...data, takmicenja: newTakmicenja}
    editData(newData);
  };

  var takmicenjaIspis = undefined;

  if (status === "pending") {
    takmicenjaIspis = (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    takmicenjaIspis = <p className="centered focused">{error}</p>;
  }
  if (
    status === "completed" &&
    (!data || !data.takmicenja || data.takmicenja.length === 0)
  ) {
    takmicenjaIspis = <NemaTakmicenja />;
  }

  if (takmicenjaIspis === undefined) {
    takmicenjaIspis = (
      <Fragment>
        <PageControl pageParams={pageParams} onChangePage={changePageHandler} />
        <ListaTakmicenja
          onRemoveTakmicenjeFromState={removeTakmicenjeFromStateHandler}
          takmicenja={data.takmicenja}
        />
      </Fragment>
    );
  }

  return (
    <Fragment>
      <Pretraga
        onResetSearchParams={resetSearchParamsHandler}
        currentSearchParams={currentSearchParams}
        onSearch={searchHandler}
      />
      {takmicenjaIspis}
    </Fragment>
  );
};
export default SvaTakmicenja;
