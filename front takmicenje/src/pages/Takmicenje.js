import { Fragment, useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import PrikazTakmicenja from "../components/takmicenja/PrikazTakmicenja";
import PrikazSvihPrijava from "../components/prijave/PrikazSvihPrijava";
import PrijavaForma from "../components/prijave/PrijavaForma";
import { useParams } from "react-router";
import useHttp from "../hooks/use-http";
import { getPrijave, getTakmicenjeIPrijave } from "../lib/api";
import LoadingSpinner from "../components/UI/LoadingSpinner";

// const DUMMY_TAKMICENJE = {
//   id: "1",
//   naziv: "Takmicenje 1",
//   mestoOdrzavanja: "Mesto1",
//   pocetak: "2021-01-01",
//   zavrsetak: "2021-02-02",
//   formatId: "1",
//   formatTip: "GRAND_SLAM",
//   brojMesta: "20",
//   brojPrijava: "10",
//   brojSlobodnihMesta: "10",
// };

// const DUMMY_PRIJAVE = [
//   {
//     id: "1",
//     datumPrijave: "2020-09-09",
//     drzavaTakmicara: "AAA",
//     email: "a@x.com",
//     takmicenjeId: "1",
//   },
//   {
//     id: "1",
//     datumPrijave: "2020-05-10",
//     drzavaTakmicara: "BBB",
//     email: "b@x.com",
//     takmicenjeId: "1",
//   },
// ];

const Takmicenje = () => {
  const [prikazFormeZaPrijavu, setPrikazFormeZaPrijavu] = useState(false);
  const prikaziFormuPrijavaHandler = () => {
    setPrikazFormeZaPrijavu((prethodniPrikazFormeZaPrijavu) => {
      return !prethodniPrikazFormeZaPrijavu;
    });
  };
  const params = useParams();
  const takmicenjeId = params.takmicenjeId;

  const { editData, sendRequest, status, data, error } = useHttp(
    getTakmicenjeIPrijave,
    true
  );

  const getPrijaveHttp = useHttp(getPrijave);

  useEffect(() => {
    sendRequest(takmicenjeId);
    getPrijaveHttp.sendRequest();
  }, [takmicenjeId]);

  const editTakmicenjeIPrijaveHandler = (prijavaData) => {
    getPrijaveHttp.sendRequest();
    var ponovljenaPrijava = [];
    if (getPrijaveHttp.data !== null) {
      ponovljenaPrijava = getPrijaveHttp.data.filter(
        (prijava) => prijava.email === prijavaData.email
      );
      if (ponovljenaPrijava.length === 0) {
        var editedData = { ...data };
        editedData.prijave.push(prijavaData);
        editData(editedData);
      }
      getPrijaveHttp.sendRequest();
    }

    // var filteredData = [];
    // filteredData = data.prijave.filter(
    //   (prijava) => prijava.email === prijavaData.email
    // );
    // if (filteredData.length === 0) {
    //   var editedData = { ...data };
    //   editedData.prijave.push(prijavaData);
    //   editData(editedData);
    // }
  };

  // const reloadPageHandler = (prijavaData) => {
  //   var filteredData = [];
  //   filteredData = data.prijave.filter(
  //     (prijava) => prijava.email === prijavaData.email
  //   );
  //   if (filteredData.length === 0) {
  //     window.location.reload();
  //   }
  // };

  var takmicenjeIspis = undefined;

  if (status === "pending") {
    takmicenjeIspis = (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    takmicenjeIspis = <p className="centered focused">{error}</p>;
  }

  if (takmicenjeIspis === undefined) {
    takmicenjeIspis = (
      <div>
        <Table striped hover variant="dark" style={{ marginTop: 5 }}>
          <thead style={{ backgroundColor: "black", color: "white" }}>
            <tr>
              <th>Naziv</th>
              <th>Mesto odrzavanja</th>
              <th>Pocetak</th>
              <th>Zavrsetak</th>
              <th>Format</th>
              <th>Broj slobodnih mesta</th>
              {window.localStorage["role"] === "ROLE_KORISNIK" && (
                <th>Prijava</th>
              )}
              {window.localStorage["role"] === "ROLE_ADMIN" && <th>Izmena</th>}
            </tr>
          </thead>
          <tbody>
            <PrikazTakmicenja
              onPrikaziFormuPrijava={prikaziFormuPrijavaHandler}
              jednoTakmicenje={true}
              takmicenje={data.takmicenje}
            />
          </tbody>
        </Table>
        <PrikazSvihPrijava prijave={data.prijave} />
        {prikazFormeZaPrijavu && (
          <PrijavaForma
            // onReloadPage={reloadPageHandler}
            onEditTakmicenjeIPrijave={editTakmicenjeIPrijaveHandler}
            takmicenjeId={params.takmicenjeId}
            takmicenjeNaziv={data.takmicenje.naziv}
          />
        )}
      </div>
    );
  }

  return <Fragment>{takmicenjeIspis}</Fragment>;
};
export default Takmicenje;
