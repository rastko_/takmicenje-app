import React, { Fragment, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Col, Form, Button } from "react-bootstrap";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import useInput from "../hooks/use-input";
import { addTakmicenje, getFormati } from "../lib/api";
import { notificationActions } from "../store/notification-slice";
import { useDispatch } from "react-redux";

const NovoTakmicenje = () => {
  const { sendRequest, error, status } = useHttp(addTakmicenje);
  const getFormatiHttp = useHttp(getFormati);
  const [neispravanPokusajSubmittovanja, setNeispravanPokusajSubmittovanja] =
    useState(false);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    getFormatiHttp.sendRequest();
  }, []);

  const proveraIspravnostiDatuma = (date1, date2) => {
    if (date2 === null) {
      var formatDate0 = new Date();
      var formatDate1 = new Date(date1);
      if (formatDate1 < formatDate0) {
        return false;
      }
      return true;
    } else {
      formatDate1 = new Date(date1);
      var formatDate2 = new Date(date2);
      if (formatDate2 < formatDate1) {
        return false;
      }
      return true;
    }
  };

  const {
    value: enteredNaziv,
    isValid: enteredNazivIsValid,
    hasError: nazivInputHasError,
    valueChangeHandler: nazivChangedHandler,
    inputBlurHandler: nazivBlurHandler,
    reset: resetNazivInput,
  } = useInput((value) => value !== "");

  const {
    value: enteredMestoOdrzavanja,
    isValid: enteredMestoOdrzavanjaIsValid,
    hasError: mestoOdrzavanjaInputHasError,
    valueChangeHandler: mestoOdrzavanjaChangedHandler,
    inputBlurHandler: mestoOdrzavanjaBlurHandler,
    reset: resetMestoOdrzavanjaInput,
  } = useInput((value) => value !== "");

  const {
    value: enteredPocetak,
    isValid: enteredPocetakIsValid,
    hasError: pocetakInputHasError,
    valueChangeHandler: pocetakChangedHandler,
    inputBlurHandler: pocetakBlurHandler,
    reset: resetPocetakInput,
  } = useInput((value) => value !== "" && proveraIspravnostiDatuma(value));

  const {
    value: enteredZavrsetak,
    isValid: enteredZavrsetakIsValid,
    hasError: zavrsetakInputHasError,
    valueChangeHandler: zavrsetakChangedHandler,
    inputBlurHandler: zavrsetakBlurHandler,
    reset: resetZavrsetakInput,
  } = useInput(
    (value) => value !== "" && proveraIspravnostiDatuma(enteredPocetak, value)
  );

  const {
    value: enteredFormatId,
    isValid: enteredFormatIdIsValid,
    hasError: formatIdInputHasError,
    valueChangeHandler: formatIdChangedHandler,
    inputBlurHandler: formatIdBlurHandler,
    reset: resetFormatIdInput,
  } = useInput((value) => value !== -1 && value > 0);

  var podnosenjeZahtevaIspis = undefined;

  if (status === "pending") {
    podnosenjeZahtevaIspis = (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    podnosenjeZahtevaIspis = (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
        }}
      >
        <p className="centered focused">{error}</p>
        <Link to={`/takmicenja`}>
          <Button variant="outline-dark" size="sm">
            Prikaz svih takmicenja
          </Button>
        </Link>
      </div>
    );
  }
  // if (status === "completed" && !error) {
  //   podnosenjeZahtevaIspis = (
  //     <div
  //       style={{
  //         margin: "auto",
  //         textAlign: "center",
  //       }}
  //     >
  //       <p className="centered focused">Uspesno kreirano takmicenje</p>
  //       <Link to={`/takmicenja`}>
  //         <Button variant="outline-dark" size="sm">
  //           Prikaz svih takmicenja
  //         </Button>
  //       </Link>
  //     </div>
  //   );
  // }

  if (status === "completed" && !error) {
    dispatch(
      notificationActions.createNotification({
        title: "USPEH",
        message: "Uspesno ste kreirali novo takmicenje",
      })
    );
    history.push("/takmicenja");
  }

  var ispisUzDugmeProvera =
    neispravanPokusajSubmittovanja ||
    nazivInputHasError ||
    mestoOdrzavanjaInputHasError ||
    pocetakInputHasError ||
    zavrsetakInputHasError ||
    formatIdInputHasError;

  const getTodayDateString = () => {
    var todayFullDate = new Date();
    var todayDateString = todayFullDate.toISOString().substring(0, 10);
    return todayDateString;
  };

  let formIsValid = false;

  if (
    enteredNazivIsValid &&
    enteredMestoOdrzavanjaIsValid &&
    enteredPocetakIsValid &&
    enteredZavrsetakIsValid &&
    enteredFormatIdIsValid
  ) {
    formIsValid = true;
  }

  const formSubmissionHandler = (e) => {
    e.preventDefault();

    if (!formIsValid) {
      setNeispravanPokusajSubmittovanja(true);
      return;
    }
    setNeispravanPokusajSubmittovanja(false);

    var addTakmicenjeData = {
      naziv: enteredNaziv,
      mestoOdrzavanja: enteredMestoOdrzavanja,
      pocetak: enteredPocetak,
      zavrsetak: enteredZavrsetak,
      formatId: enteredFormatId,
    };
    sendRequest(addTakmicenjeData);

    resetNazivInput();
    resetMestoOdrzavanjaInput();
    resetPocetakInput();
    resetZavrsetakInput();
    resetFormatIdInput();
  };

  if (getFormatiHttp.error) {
    return (
      <div
        style={{
          margin: "auto",
          textAlign: "center",
        }}
      >
        <p className="centered focused">
          Trenutno ne mozete da kreirate novo takmicenje
        </p>
        <Link to={`/takmicenja`}>
          <Button variant="outline-dark" size="sm">
            Prikaz svih takmicenja
          </Button>
        </Link>
      </div>
    );
  }

  return (
    <Fragment>
      <Form style={{ marginTop: 35 }} onSubmit={formSubmissionHandler}>
        <Col>
          <Form.Group>
            <Form.Label>Naziv takmicenja</Form.Label>
            <Form.Control
              id="naziv"
              name="naziv"
              value={enteredNaziv}
              onChange={nazivChangedHandler}
              onBlur={nazivBlurHandler}
              isInvalid={nazivInputHasError}
              as="input"
              type="text"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Naziv takmicenja ne moze da bude prazno polje.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Mesto odrzavanja</Form.Label>
            <Form.Control
              id="mestoOdrzavanja"
              name="mestoOdrzavanja"
              value={enteredMestoOdrzavanja}
              onChange={mestoOdrzavanjaChangedHandler}
              onBlur={mestoOdrzavanjaBlurHandler}
              isInvalid={mestoOdrzavanjaInputHasError}
              as="input"
              type="text"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Mesto odrzavanja ne moze da bude prazno polje.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Datum pocetka takmicenja</Form.Label>
            <Form.Control
              id="pocetak"
              name="pocetak"
              value={enteredPocetak}
              onChange={pocetakChangedHandler}
              onBlur={pocetakBlurHandler}
              isInvalid={pocetakInputHasError}
              min={getTodayDateString()}
              as="input"
              type="date"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Pocetak ne moze da bude nedefinisan, niti datum pre danasnjeg
              dana.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Datum zavrsetka takmicenja</Form.Label>
            <Form.Control
              disabled={enteredPocetak === ""}
              id="zavrsetak"
              name="zavrsetak"
              value={enteredZavrsetak}
              onChange={zavrsetakChangedHandler}
              onBlur={zavrsetakBlurHandler}
              isInvalid={zavrsetakInputHasError}
              min={enteredPocetak}
              as="input"
              type="date"
            ></Form.Control>
            <Form.Control.Feedback type="invalid" tooltip>
              Zavrsetak ne moze da bude nedefinisan, niti datum pre dana pocetka
              takmicenja.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Format takmicenja</Form.Label>
            {!getFormatiHttp.error && getFormatiHttp.status === "completed" && (
              <Form.Control
                value={enteredFormatId}
                onChange={formatIdChangedHandler}
                onBlur={formatIdBlurHandler}
                isInvalid={formatIdInputHasError}
                id="formatId"
                name="formatId"
                as="select"
              >
                <option value={-1}></option>
                {getFormatiHttp.data.map((format) => (
                  <option value={format.id} key={format.id}>
                    {format.tip}
                  </option>
                ))}
              </Form.Control>
            )}

            <Form.Control.Feedback type="invalid" tooltip>
              Odaberite jedan od ponudjenih formata.
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <br />
        <Col>
          <Form.Group>
            <Button variant="secondary" type="submit">
              Podnesi zahtev za kreiranje novog takmicenja
            </Button>
            {ispisUzDugmeProvera && (
              <span style={{ color: "red" }}>
                {" "}
                Niste ispravno uneli podatke
              </span>
            )}
          </Form.Group>
        </Col>
      </Form>
      {podnosenjeZahtevaIspis}
    </Fragment>
  );
};
export default NovoTakmicenje;
