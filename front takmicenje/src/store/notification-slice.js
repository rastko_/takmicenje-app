import { createSlice } from "@reduxjs/toolkit";

const defaultNotificationState = {
  notificationIsVisible: false,
  notification: null,
};

const notificationSlice = createSlice({
  name: "notification",
  initialState: defaultNotificationState,
  reducers: {
    createNotification(state, action) {
      state.notificationIsVisible = true;
      state.notification = {
        title: action.payload.title,
        message: action.payload.message,
      };
    },
    removeNotification(state) {
      state.notificationIsVisible = false;
      state.notification = null;
    },
  },
});

export const notificationActions = notificationSlice.actions;

export default notificationSlice;
