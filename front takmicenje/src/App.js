import React, { Fragment } from "react";
import { useSelector } from "react-redux";

import AppJSX from "./components/layout/AppJSX";
import Layout from "./components/layout/Layout";
import Notification from "./components/UI/Notification";

function App() {
  const notificationVisibility = useSelector(
    (state) => state.notification.notificationIsVisible
  );
  const notification = useSelector((state) => state.notification.notification);

  return (
    <Fragment>
      {notificationVisibility && (
        <Notification
          title={notification.title}
          message={notification.message}
        />
      )}

      <Layout>
        <AppJSX />
      </Layout>
    </Fragment>
  );
}

export default App;
