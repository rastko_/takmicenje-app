import { useReducer, useCallback } from "react";

function httpReducer(state, action) {
  if (action.type === "SEND") {
    return {
      data: null,
      error: null,
      status: "pending",
    };
  }

  if (action.type === "SUCCESS") {
    return {
      data: action.responseData,
      error: null,
      status: "completed",
    };
  }

  if (action.type === "ERROR") {
    return {
      data: null,
      error: action.errorMessage,
      status: "completed",
    };
  }

  if (action.type === "REMOVE_DATA") {
    console.log(state.data.length)
    if (state.data.length > 0) {
      const filteredData = state.data.filter((obj) => obj.id !== action.dataId);
      return {
        data: filteredData,
        error: state.error,
        status: state.status,
      };
    }
    console.log(state.data)
  }

  if (action.type === "EDIT_DATA") {
    return {
      data: action.data,
      error: state.error,
      status: state.status
    }
  }

  return state;
}

function useHttp(requestFunction, startWithPending = false) {
  const [httpState, dispatch] = useReducer(httpReducer, {
    status: startWithPending ? "pending" : null,
    data: null,
    error: null,
  });

  const sendRequest = useCallback(
    async function (requestData) {
      dispatch({ type: "SEND" });
      try {
        const responseData = await requestFunction(requestData);
        dispatch({ type: "SUCCESS", responseData });
      } catch (error) {
        dispatch({
          type: "ERROR",
          errorMessage: error.message || "Something went wrong!",
        });
      }
    },
    [requestFunction]
  );

  const removeData = (dataId) => {
    dispatch({ type: "REMOVE_DATA", dataId });
  };

  const editData = (data) => {
    dispatch({ type: "EDIT_DATA", data });
  };

  return {
    editData,
    removeData,
    sendRequest,
    ...httpState,
  };
}

export default useHttp;
